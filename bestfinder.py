import re
import requests
from bs4 import BeautifulSoup as soup

# Set queries
search_query="what is the best bicep exercise?"
keyword="curls"

# Get sites
sites=[]
search_result = requests.get("https://searx.isan.ro/search?q="+search_query)
webpage = soup(search_result.text, "html.parser")
results = webpage.find_all('h3')
for result in results:
    link = result.find('a').get('href')
    sites.append(link)
    # print(link)

# Scrape sites
print("Number of websites to check: " + str(len(sites)) + "\n")
nr_sites = 0.0
nr_mentions = 0.0
for site in sites:
    nr_sites += 1
    try:
        page = requests.get(site, allow_redirects=False).text
        mentions = page.count(keyword) + page.count(keyword.capitalize())
        if mentions > 0:
            nr_mentions += 1
        print("{0} -> {1} \n".format(site, mentions))
    except:
        sites.remove(site)
        print("Skipped {0} \n".format(site))

print(str(nr_mentions/nr_sites*100) + "%")

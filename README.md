# bestfinder
Simple tool to check how often a certain keyword comes up in the search results for a certain query.

Set a search query and keyword and the program will return the percent of results in which the keyword was mentioned at least once. You can use this to check roughly how popular a certain thing is.

Example:

"""

query: "best bicep exercise"
keyword: "curls"

result: 98.9899% ("curls" was mentioned in this many of the checked webpages)

"""

Note: Sites that block scraping may cause false negatives
